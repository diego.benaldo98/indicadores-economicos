import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GraficoIndicadoresComponent} from './grafico-indicadores/grafico-indicadores.component';
import {NgChartsModule} from "ng2-charts";


@NgModule({
  exports: [
    GraficoIndicadoresComponent
  ],
  declarations: [
    GraficoIndicadoresComponent
  ],
  imports: [
    CommonModule,
    NgChartsModule
  ]
})
export class ChartsModule {
}
