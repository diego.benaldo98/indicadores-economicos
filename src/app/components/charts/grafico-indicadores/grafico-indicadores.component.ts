import {Component, Input, OnInit} from '@angular/core';
import {ChartDataset} from 'chart.js';
import * as moment from 'moment';
import {Indicador} from '../../../models/indicador';
import {Serie} from '../../../models/serie';

@Component({
  selector: 'app-grafico-indicadores',
  templateUrl: './grafico-indicadores.component.html',
  styleUrls: ['./grafico-indicadores.component.scss']
})
export class GraficoIndicadoresComponent implements OnInit {

  @Input() indicador: Indicador;

  datosVertical: ChartDataset[] = [];
  datosHorizontal: String[] = [];


  constructor() {
  }

  ngOnInit(): void {
    this.armarDatosGrafico();
    console.log(this.indicador);
  }

  public armarDatosGrafico(): void {
    this.datosVertical = [
      {
        data: this.obtenerPrecios,
        label: this.indicador.nombre
      }
    ];
    this.datosHorizontal = this.obtenerFechas;
  }

  get series(): Serie[] {
    return this.indicador.serie;
  }

  get obtenerPrecios(): number[] {
    return this.series.map(serie => serie.valor);
  }

  get obtenerFechas(): string[] {
    return this.series.map(serie => moment(serie.fecha).format('DD-MM-YYYY'));
  }

}
