import {Component, OnInit, Input} from '@angular/core';
import {Indicador} from '../../../models/indicador';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {MiIndicadorService} from '../../../services/mi-indicador.service';
import {Serie} from '../../../models/serie';
import {ToastService} from '../../../services/toast.service';

@Component({
  selector: 'app-detalle-indicador',
  templateUrl: './detalle-indicador.component.html',
  styleUrls: ['./detalle-indicador.component.scss']
})
export class DetalleIndicadorComponent implements OnInit {

  @Input() indicador: Indicador;
  @Input() referenciaModal: NgbModalRef;

  public series: Serie[] = [];
  public estadoCargando = false;
  public estadoError = false;

  constructor(
    private miIndicadorService: MiIndicadorService,
    private toastService: ToastService
  ) {
  }

  ngOnInit(): void {
    this.obtenerSeriesService();
  }

  public obtenerSeriesService(): void {
    this.estadoCargando = true;
    this.miIndicadorService.obtenerSeriesPorCodigoDeIndicador(this.indicador.codigo, 10).subscribe({
      next: series => {
        this.indicador.serie = series
      },
      error: error => {
        console.log(`Error al consumit servicio => ${error}`);
        this.toastService.errorCargarDatos();
        this.estadoCargando = false;
        this.estadoError = true;
      },
      complete: () => {
        this.estadoCargando = false;
        this.toastService.datosCargadosCorrectamente();
      }
    });
  }
}
