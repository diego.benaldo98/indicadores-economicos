import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';
import { PriceComponent } from './price/price.component';

@NgModule({
  exports: [
    LoadingComponent,
    PriceComponent
  ],
  declarations: [
    LoadingComponent,
    PriceComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
