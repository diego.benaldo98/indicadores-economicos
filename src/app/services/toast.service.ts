import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    private toastrService: ToastrService
  ) {
  }

  public datosCargadosCorrectamente(texto = 'Datos cargados correctamente'): void {
    this.toastrService.success(texto, 'Aviso');
  }

  public errorCargarDatos(texto = 'No se pudo cargar información'): void {
    this.toastrService.error(texto, 'Error');
  }
}
