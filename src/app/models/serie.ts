export class Serie {
  fecha: Date;
  valor: number;

  public constructor(init?: Partial<Serie>) {
    Object.assign(this, init);
  }
}
